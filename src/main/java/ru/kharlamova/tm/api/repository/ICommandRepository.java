package ru.kharlamova.tm.api.repository;

import ru.kharlamova.tm.model.Command;

public interface ICommandRepository {

    Command[] getTerminalCommands();

}
