package ru.kharlamova.tm.api.repository;

import ru.kharlamova.tm.model.Task;

import java.util.List;

public interface ITaskRepository {

    List<Task> findAll();

    List<Task> findAllByProjectId(String projectId);

    void removeAllByProjectId(String projectId);

    Task bindTaskByProject(String taskId, String projectId);

    Task unbindTaskFromProject(String projectId);

    void add(Task task);

    void remove(Task task);

    void clear();

    Task findOneById(String id);

    Task removeOneById(String id);

    Task findOneByIndex(Integer index);

    Task findOneByName(String name);

    Task removeOneByIndex(Integer index);

    Task removeOneByName(String name);

}
