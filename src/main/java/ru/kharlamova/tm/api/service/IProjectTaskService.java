package ru.kharlamova.tm.api.service;

import ru.kharlamova.tm.model.Project;
import ru.kharlamova.tm.model.Task;

import java.util.List;

public interface IProjectTaskService {

    // TASK CONTROLLER
    List<Task> findAllTaskByProjectId(String projectId);

    // TASK CONTROLLER
    Task bindTaskByProject(String taskId, String projectId);

    // TASK CONTROLLER
    Task unbindTaskFromProject(String taskId);

    // PROJECT CONTROLLER
    Project removeProjectById(String projectId);

}
