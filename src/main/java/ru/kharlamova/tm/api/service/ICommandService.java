package ru.kharlamova.tm.api.service;

import ru.kharlamova.tm.model.Command;

public interface ICommandService {

    Command[] getTerminalCommands();

}
