package ru.kharlamova.tm.service;

import ru.kharlamova.tm.api.repository.ICommandRepository;
import ru.kharlamova.tm.api.service.ICommandService;
import ru.kharlamova.tm.model.Command;

public class CommandService implements ICommandService {

    private final ICommandRepository commandRepository;

    public CommandService(ICommandRepository commandRepository) {
        this.commandRepository = commandRepository;
    }

    @Override
    public Command[] getTerminalCommands() {
        return commandRepository.getTerminalCommands();
    }

}
